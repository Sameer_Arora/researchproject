function [out] = texture_feat(img)

img= int32(rgb2gray(img));

[m, n]=size(img);

%Spatial-Dependence Matrices with d=1;
p_0 = zeros(256,256);
p_90 = zeros(256,256);
p_45 = zeros(256,256);
p_135 = zeros(256,256);



% p_0 is in direction of horizontal.
for i=1:m
    for j=1:n-1
        
        p_0( img(i,j) +1 , img(i,j+1) +1 )=p_0( img(i,j)+1, img(i,j+1) +1 )+1;
        p_0( img(i,j+1)+1, img(i,j)+1  )=p_0(   img(i,j+1)+1 , img(i,j)+1  )+1; 
        
    end
end

% p_90 is in direction of vertical.
for i=1:m-1
    for j=1:n
        
        p_90( img(i,j) +1, img(i+1,j)+1 )=p_90( img(i,j)+1 , img(i+1,j) +1)+1;
        p_90( img(i+1,j)+1, img(i,j) +1 )=p_90(   img(i+1,j)+1 , img(i,j) +1 )+1; 
        
    end
end

% p_45 is in direction of left diagnol.
for i=2:m
    for j=2:n
        
        p_45( img(i,j) +1, img(i-1,j-1)+1 )=p_45( img(i,j)+1 , img(i-1,j-1)+1 )+1;
        p_45( img(i-1,j-1)+1, img(i,j) +1 )=p_45(   img(i-1,j-1) +1, img(i,j) +1 )+1; 
        
    end
end

% p_135 is in direction of right diagnol.

for i=1:m-1
    for j=1:n-1
        
        p_135( img(i,j)+1, img(i+1,j+1)+1 )=p_135( img(i,j) +1, img(i+1,j+1)+1 )+1;
        p_135( img(i+1,j+1)+1 , img(i,j)+1  )=p_135(   img(i+1,j+1) +1, img(i,j) +1 )+1; 
        
    end
end


% sum(sum(p_0))
% (2* m*(n-1) )

assert( isequal (    sum(sum(p_0)) , (2* m*(n-1) ) ) ) ;
assert( isequal (    sum(sum(p_90)) , (2* (m-1)*n ) ) ) ;
assert( isequal (    sum(sum(p_45)) , (2* (m-1)*(n-1) ) ) ) ;
assert( isequal (    sum(sum(p_135)) , (2* (n-1)*(m-1) ) ) ) ;


assert( isequal (    p_0 , p_0' ) ) ;
assert( isequal (    p_90 , p_90' ) ) ;
assert(isequal (    p_45 , p_45' ) ) ;
assert(isequal (    p_135 , p_135' ) ) ;

p_0 = p_0/ (2* m*(n-1) ) ;
p_90 = p_90 / (2* (m-1)*n )  ;
p_45 = p_45 / (2* (m-1)*(n-1) ) ;
p_135 = p_135/(2* (m-1)*(n-1) );

% 
% valueSet = {'0','90','45','135'};
% keySet = [1 2 3 4];
% M = containers.Map(keySet,valueSet);

% textutre features 
f1 = zeros(1,5);  % homogenity.

for j=1:5
    if(j==1)
        f1(j)= sum(sum(p_0.^2)) ; 
    elseif(j==2)
        f1(j)= sum(sum(p_90.^2)) ; 
    elseif(j==3)
        f1(j)= sum(sum(p_45.^2)) ; 
    elseif(j==4)
        f1(j)= sum(sum(p_135.^2)) ;
    elseif(j==5)
        f1(j)= sum( f1(1:4) ) / 4; 
    end
end

f2 = zeros(1,5);


for j=1:5
    if(j==1)
        f2(j)= contrast(p_0) ; 
       
    elseif(j==2)
         
        f2(j)= contrast(p_90) ; 
       
     
    elseif(j==3)
        f2(j)= contrast(p_45) ; 
       
    elseif(j==4)
       f2(j)= contrast(p_135) ; 
       
    elseif(j==5)
        
        f2(j)= sum ( f2 (1:4 ))/4 ; 
        
    end
end

f3 = zeros(1,5);

for j=1:5
    if(j==1)
        f3(j)= auto_correlation(p_0);
    elseif(j==2)
        f3(j)= auto_correlation(p_90);
    elseif(j==3)
        f3(j)= auto_correlation(p_45);
    elseif(j==4)
        f3(j)=auto_correlation(p_135);
    elseif(j==5)
        f3(j)= sum( f3(1:4) ) / 4 ;
    end
end

f4 = zeros(1,5); 

for j=1:5
    if(j==1)
        f4(j)= variance(p_0);
    elseif(j==2)
        f4(j)= variance(p_90);
    elseif(j==3)
        f4(j)= variance(p_45);
    elseif(j==4)
        f4(j)=variance(p_135);
    elseif(j==5)
        f4(j)= sum( f4(1:4) ) / 4 ;
    end
end

f5 = zeros(1,5); 

for j=1:5
    if(j==1)
        f5(j)= inv_diff_moment(p_0);
    elseif(j==2)
        f5(j)= inv_diff_moment(p_90);
    elseif(j==3)
        f5(j)= inv_diff_moment(p_45);
    elseif(j==4)
        f5(j)=inv_diff_moment(p_135);
    elseif(j==5)
        f5(j)= sum( f5(1:4) ) / 4 ;
    end
end


f6 = zeros(1,5); 

for j=1:5
    if(j==1)
        f6(j)= sum_avg(p_0);
    elseif(j==2)
        f6(j)= sum_avg(p_90);
    elseif(j==3)
        f6(j)= sum_avg(p_45);
    elseif(j==4)
        f6(j)=sum_avg(p_135);
    elseif(j==5)
        f6(j)= sum( f6(1:4) ) / 4 ;
    end
end


f8 = zeros(1,5); 

for j=1:5
    if(j==1)
        f8(j)= sum_entropy(p_0);
    elseif(j==2)
        f8(j)= sum_entropy(p_90);
    elseif(j==3)
        f8(j)= sum_entropy(p_45);
    elseif(j==4)
        f8(j)=sum_entropy(p_135);
    elseif(j==5)
        f8(j)= sum( f8(1:4) ) / 4 ;
    end
end

f7 = zeros(1,5); 

for j=1:5
    if(j==1)
        f7(j)= sum_var(p_0,f8(j));
    elseif(j==2)
        f7(j)= sum_var(p_90,f8(j));
    elseif(j==3)
        f7(j)= sum_var(p_45,f8(j));
    elseif(j==4)
        f7(j)=sum_var(p_135,f8(j));
    elseif(j==5)
        f7(j)= sum( f7(1:4) ) / 4 ;
    end
end

f9 = zeros(1,5); 

for j=1:5
    if(j==1)
        f9(j)= entopy(p_0);
    elseif(j==2)
        f9(j)= entopy(p_90);
    elseif(j==3)
        f9(j)= entopy(p_45);
    elseif(j==4)
        f9(j)=entopy(p_135);
    elseif(j==5)
        f9(j)= sum( f9(1:4) ) / 4 ;
    end
end



f10 = zeros(1,5); 

for j=1:5
    if(j==1)
        f10(j)= diff_var(p_0);
    elseif(j==2)
        f10(j)= diff_var(p_90);
    elseif(j==3)
        f10(j)= diff_var(p_45);
    elseif(j==4)
        f10(j)=diff_var(p_135);
    elseif(j==5)
        f10(j)= sum( f10(1:4) ) / 4 ;
    end
end



f11 = zeros(1,5); 

for j=1:5
    if(j==1)
        f11(j)= diff_entropy(p_0);
    elseif(j==2)
        f11(j)= diff_entropy(p_90);
    elseif(j==3)
        f11(j)= diff_entropy(p_45);
    elseif(j==4)
        f11(j)=diff_entropy(p_135);
    elseif(j==5)
        f11(j)= sum( f11(1:4) ) / 4 ;
    end
end


f12 = zeros(1,5); 

for j=1:5
    if(j==1)
        f12(j)= f_12(p_0,f9(j));
    elseif(j==2)
        f12(j)= f_12(p_90 ,f9(j) );
    elseif(j==3)
        f12(j)= f_12(p_45 ,f9(j));
    elseif(j==4)
        f12(j)=f_12(p_135,f9(j) );
    elseif(j==5)
        f12(j)= sum( f12(1:4) ) / 4 ;
    end
end


f13 = zeros(1,5); 

for j=1:5
    if(j==1)
        f13(j)= f_13(p_0 ,f9(j));
    elseif(j==2)
        f13(j)= f_13(p_90 ,f9(j) );
    elseif(j==3)
        f13(j)= f_13(p_45 ,f9(j));
    elseif(j==4)
        f13(j)=f_13(p_135 ,f9(j));
    elseif(j==5)
        f13(j)= sum( f13(1:4) ) / 4 ;
    end
end

out= [f1' f2' f3' f4' f5' f6' f7' f8' f9' f10' f11' f12' f13'];

out=out(end,:);
end