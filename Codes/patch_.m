function ca = patch_ (rgbImage ,patch_x,patch_y,plot)

%close all;  % Close all figures (except those of imtool.)

% Test code if you want to try it with a gray scale image.
% Uncomment line below if you want to see how it works with a gray scale image.
% rgbImage = rgb2gray(rgbImage);

%figH=figure;

% Display image full screen.
%imagesc(rgbImage);

% Enlarge figure to full screen.
% set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
% drawnow;
% Get the dimensions of the image.  numberOfColorBands should be = 3.
[rows, columns, numberOfColorBands] = size(rgbImage) ;

%==========================================================================
% The first way to divide an image up into blocks is by using mat2cell().
blockSizeR = patch_y; % Rows in block.
blockSizeC = patch_x ; % Columns in block.


% Figure out the size of each block in rows.

% Most will be blockSizeR but there may be a remainder amount of less than that.
wholeBlockRows = floor(rows / blockSizeR);

% if rem(rows, blockSizeR) ~=0
%     blockVectorR = [blockSizeR * ones(1, wholeBlockRows), rem(rows, blockSizeR)];
% else 
%     blockVectorR = [blockSizeR * ones(1, wholeBlockRows)];
% end

blockVectorR = [blockSizeR * ones(1, wholeBlockRows)];
    
    % Figure out the size of each block in columns.
wholeBlockCols = floor(columns / blockSizeC);
% if rem(columns, blockSizeC) ~=0
%     blockVectorC = [blockSizeC * ones(1, wholeBlockCols), rem(columns, blockSizeC)];
% else
%     blockVectorC = [blockSizeC * ones(1, wholeBlockCols)];
% end

blockVectorC = [blockSizeC * ones(1, wholeBlockCols)];

% cropping the rgb image
rgbImage =rgbImage( 1:wholeBlockRows*blockSizeR ,1: wholeBlockCols* blockSizeC,: ) ;

% Create the cell array, ca. 
% Each cell (except for the remainder cells at the end of the image)
% in the array contains a blockSizeR by blockSizeC by 3 color array.
% This line is where the image is actually divided up into blocks.
if numberOfColorBands > 1
    % It's a color image.
    ca = mat2cell(rgbImage, blockVectorR, blockVectorC, numberOfColorBands);
else
    ca = mat2cell(rgbImage, blockVectorR, blockVectorC);
end

if(plot==1)
    
    set(figH,'Name',[ num2str(wholeBlockCols*wholeBlockRows) 'patchs  with '  num2str(blockSizeR) ' rows n ' num2str(blockSizeC) ' cols'],'NumberTitle','off') ;

    % Now display all the blocks.
    plotIndex = 1;
    numPlotsR = size(ca, 1);
    numPlotsC = size(ca, 2);

    for r = 1 : numPlotsR
        for c = 1 : numPlotsC
            fprintf('plotindex = %d,   c=%d, r=%d\n', plotIndex, c, r);
            % Specify the location for display of the image.
            subplot(numPlotsR, numPlotsC, plotIndex);
            % Extract the numerical array out of the cell
            % just for tutorial purposes.
            rgbBlock = ca{r,c};
            imagesc(rgbBlock); % Could call imshow(ca{r,c}) if you wanted to.
            axis off;
            [~, ~ , ~] = size(rgbBlock);
            % Make the caption the block number.
            caption = sprintf('#%d', ...
                plotIndex);
            title(caption);
            drawnow;
            % Increment the subplot to the next location.
            plotIndex = plotIndex + 1;
        end
    end

end

% % Display the original image in the upper left.
% subplot(4, 6, 1);
% imshow(rgbImage);
% title('Original Image');

end