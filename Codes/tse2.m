function feat= gabor_feat(img)

%     close all;
%     img=imread('../output/fal_neg_patch/pat1.jpg');
    [m n]=size(img);

    gaborArray = gaborFilterBank(m);

    feat= gaborFeatures_patch(img, gaborArray);

end