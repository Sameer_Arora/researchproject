function K=kernel_RBF_mod(Img,value)

% RBF-kernel parameters
a = 2;
b = 1;
sigma = .5;

sz=size(Img);

for k=1:sz(3)
    Kint(:,:,k)=Img(:,:,k)-value(k);
end

Kint=Kint.^a;
Kint=squeeze(sum(Kint,3));
K=exp(-Kint/sigma^2);
    
end