function c= contrast (p )
    sum=0;
    for k=1:256-1
                p_sum=0;
                for i1= 1: 256-k
                    p_sum= p_sum + 2 * p(i1,i1+ k)  ;
                end
                p_sum= p_sum * k.^2;
                sum= sum+p_sum;
    end

    c=sum;
end