% Performs anisotropic diffusion on images.
% Smoothes the images while keeping edges sharp.
% Uses a function code by Peter Kovesi to do the diffusion.
% http://www.csse.uwa.edu.au
function anisotropic_diffusion()
clc;    % Clear the command window.
workspace;  % Make sure the workspace panel is showing.
fontSize = 18;


% Read in a standard MATLAB color demo image.
folder = fullfile('/home/joohi/Downloads/burns/Telemedicine/filter_img/');
filePattern=fullfile(folder,'m*.jpg');
jpgFiles= dir(filePattern);
prefix='m';
flieformate='.jpg';
for i=1:length(jpgFiles)
  
    baseFileName = strcat(prefix,num2str(i),flieformate);
    %figure();
    %imshow(baseFileName);
    % baseFileName = 'peppers.png';
    % baseFileName = 'hestain.png';
    % baseFileName = 'autumn.tif';
    % Get the full filename, with path prepended.
    fullFileName = fullfile(folder, baseFileName);
    if ~exist(fullFileName, 'file')
	% Didn't find it there.  Check the search path for it.
	fullFileName = baseFileName; % No path this time.
	if ~exist(fullFileName, 'file')
		% Still didn't find it.  Alert user.
		errorMessage = sprintf('Error: %s does not exist.', fullFileName);
		uiwait(warndlg(errorMessage));
		return;
	end
    end
    rgbImage = imread(fullFileName);
    %imshow(rgbImage);
    % Get the dimensions of the image.  numberOfColorBands should be = 3.
    [rows, columns, numberOfColorBands] = size(rgbImage);
    % Display the original color image.
    %subplot(2, 2, 3);
    %imshow(rgbImage, []);
    %title('Original Color Image', 'FontSize', fontSize);
    %drawnow;
    % Extract the individual red, green, and blue color channels.
    redChannel = rgbImage(:, :, 1);
    greenChannel = rgbImage(:, :, 2);
    blueChannel = rgbImage(:, :, 3);

    niter = 20;
    kappa = 8;
    lambda = 0.25;
    option = 1;
    diffR = anisodiff(redChannel, niter, kappa, lambda, option);
    diffG = anisodiff(greenChannel, niter, kappa, lambda, option);
    diffB = anisodiff(blueChannel, niter, kappa, lambda, option);
    rgbOutput = uint8(cat(3, diffR, diffG, diffB));
    path= '/home/joohi/Downloads/burns/Telemedicine/Anisotropic_mat_img/a';
    imwrite(rgbOutput,[path, num2str(i),'.jpg']);

end
% % Display the diffused images.
% subplot(2, 2, 4);
% imshow(diffR, []);
% title('Diffused Red Channel Image', 'FontSize', fontSize);
% msgbox('Showing the diffused Red Channel.  Click OK to see the Green Channel.');
% figure();
% imshow(diffG, []);
% title('Diffused Green Channel Image', 'FontSize', fontSize);
% msgbox('Showing the diffused Green Channel.  Click OK to see the Blue Channel.');
% figure();
% imshow(diffB, []);
% title('Diffused Blue Channel Image', 'FontSize', fontSize);
% msgbox('Showing the diffused Blue Channel.  Click OK to see the full color diffused image.');
% figure();
% imshow(rgbOutput);
% title('Diffused Color Image', 'FontSize', fontSize);

% ANISODIFF - Anisotropic diffusion.
%
% Usage:
%  diff = anisodiff(im, niter, kappa, lambda, option)
%
% Arguments:
%         im     - input image
%         niter  - number of iterations.
%         kappa  - conduction coefficient 20-100 ?
%         lambda - max value of .25 for stability
%         option - 1 Perona Malik diffusion equation No 1
%                  2 Perona Malik diffusion equation No 2
%
% Returns:
%         diff   - diffused image.
%
% kappa controls conduction as a function of gradient.  If kappa is low
% small intensity gradients are able to block conduction and hence diffusion
% across step edges.  A large value reduces the influence of intensity
% gradients on conduction.
%
% lambda controls speed of diffusion (you usually want it at a maximum of
% 0.25)
%
% Diffusion equation 1 favours high contrast edges over low contrast ones.
% Diffusion equation 2 favours wide regions over smaller ones.

% Reference: 
% P. Perona and J. Malik. 
% Scale-space and edge detection using ansotropic diffusion.
% IEEE Transactions on Pattern Analysis and Machine Intelligence, 
% 12(7):629-639, July 1990.
%
% Peter Kovesi  
% School of Computer Science & Software Engineering
% The University of Western Australia
% pk @ csse uwa edu au
% http://www.csse.uwa.edu.au
%
% June 2000  original version.       
% March 2002 corrected diffusion eqn No 2.

function diff = anisodiff(im, niter, kappa, lambda, option)

if ndims(im)==3
	error('Anisodiff only operates on 2D grey-scale images');
end

im = double(im);
[rows,cols] = size(im);
diff = im;
  
for i = 1:niter
	%  fprintf('\rIteration %d',i);

	% Construct diffl which is the same as diff but
	% has an extra padding of zeros around it.
	diffl = zeros(rows+2, cols+2);
	diffl(2:rows+1, 2:cols+1) = diff;

	% North, South, East and West differences
	deltaN = diffl(1:rows,2:cols+1)   - diff;
	deltaS = diffl(3:rows+2,2:cols+1) - diff;
	deltaE = diffl(2:rows+1,3:cols+2) - diff;
	deltaW = diffl(2:rows+1,1:cols)   - diff;

	% Conduction
	if option == 1
		% Diffusion equation 1 favours high contrast edges over low contrast ones.
		cN = exp(-(deltaN/kappa).^2);
		cS = exp(-(deltaS/kappa).^2);
		cE = exp(-(deltaE/kappa).^2);
		cW = exp(-(deltaW/kappa).^2);
	elseif option == 2
		% Diffusion equation 2 favours wide regions over smaller ones.
		cN = 1./(1 + (deltaN/kappa).^2);
		cS = 1./(1 + (deltaS/kappa).^2);
		cE = 1./(1 + (deltaE/kappa).^2);
		cW = 1./(1 + (deltaW/kappa).^2);
	end

  diff = diff + lambda*(cN.*deltaN + cS.*deltaS + cE.*deltaE + cW.*deltaW);

	%  Uncomment the following to see a progression of images
% 	subplot(ceil(sqrt(niter)),ceil(sqrt(niter)), i)
% 	imagesc(diff);
% 	colormap(gray);
% 	axis image;
end



