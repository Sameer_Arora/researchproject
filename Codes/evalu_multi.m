function [ acc pre rec jac_ind dic_coeff, F1] = evalu_multi( res , gr_truth ,no_classes)
    
  tp=zeros(no_classes,1);
  fp=zeros(no_classes,1);
  tn=zeros(no_classes,1);
  fn=zeros(no_classes,1);
  
  % rows denote the pedicted calsses and columns the actual classes.
  
  occurences= zeros(no_classes,no_classes);
  
  for k=0:no_classes-1
        
      for j=0:no_classes-1
            
            if k==j
                bool1=(res==k );
                bool2=( gr_truth ==k );
                
                occurences(k+1,j+1) = nnz( bool1 & bool2 ) ;
                
            else
                bool1=(res==k );
                bool2=(gr_truth==j );
                
                occurences(k+1,j+1) = nnz ( bool1 & bool2 ) ;
               
            end
        end
  end
    
 acc=0;
 pre=0;
 rec=0;
 F1=0;
 jac_ind=0;
 dic_coeff=0;
 occurences;
 
  for k=1:no_classes
      tp(k)= occurences(k,k);
      fp(k)= sum(occurences(k,:))- tp(k);
      fn(k)= sum(occurences(:,k))- tp(k);
      tn(k)= sum(sum(occurences)) - fp(k) - fn(k) -tp(k)
      acc = acc + ( tp(k)+ tn(k) )/( tp(k)+ tn(k) +fn(k) +fp(k) );
      
      if( tp(k)+ fp(k) )~=0
        pre = pre +( tp(k) )/( tp(k)+ fp(k) );
      end
      
      rec = rec +( tp(k) )/( tp(k)+ fn (k) ) ;
      jac_ind= jac_ind + tp(k) / (tp(k)+fp(k)+fn(k));
      dic_coeff=dic_coeff + 2* tp(k) /( 2*tp(k) +fp(k)+ fn(k) );
    
  end
    
  acc=acc/no_classes;
  pre=pre/no_classes;
  rec=rec/no_classes;
  jac_ind=jac_ind/no_classes;
  dic_coeff=dic_coeff/no_classes;
  F1= 2* pre *rec /(pre+rec) ;

end