 
data=rgb2gray(imread('images/b2.jpg'));
data=double(data);

data= reshape(data,size(data,1)*size(data,2),1);

sigma=10;

m = size(data,1);
    
    %% Step 1 of ALgo compute Affinity matrix using Gaussian .
    
    A= zeros(m,m);
    Dis=zeros(m,m);
    
    for i=1:m
        for j=1:m
            dist = norm ( data(i,:) - data(j,:) ).^ 2  ;
            Dis(i,j)=dist;
            if i~=j
                A(i,j)= exp( - dist / (2*sigma^2) ) ;
            else
                A(i,j)=0;
            end
        end
    end
   
%     A
%     Dis
%     pause
      
    %%  Step 2 of ALgo compute Diagnol Matrix matrix and L matrix .
    
    
    D = diag( sum(A,1) .^ (-1/2)  );
    L = D  * A * D ;
    
   % pause
    
    %%  Step 3 of ALgo compute k largest eigenvectors of  L matrix .
    [Evec Eval ]=eig(L);
    
    Inter= abs(diag(Eval)) ;
    [ Inter indx ] =sort(Inter,'descend') ;
    
    X = Evec(:, indx(1:k) );
    
    k=2;
    
    
    indx
    Evec
    X
    pause
    
    %%  Step 4 of ALgo renormalize the X matrix .
    
    
    % sum( X.^2 ,2 );
    temp_mat= sum( X.^2 ,2 ) .^ (1/2) ;
    temp_mat = repmat (temp_mat, 1, k ) ;
    
    Y = X ./ temp_mat ;
    
    % Y our new data points .
    
     %%  Step 5 of ALgo  Apply K means on Y .
     
    ini_centers = kMeansInitCentroids( Y, k );
    
   [centers idx ]= runkMeans(Y,ini_centers,10,true);
    
    
    fprintf('\nK-Means Done.\n\n');

    fprintf('Program paused. Press enter to continue.\n');
    %pause;

   %%  Step 6 of ALgo  assign cluster to the datasets .
   
   thres = zeros(k,1);
   step= (255-0) / (k-1) ;
   thres= [ 0: step :255 ]
   
   out_img = thres(:,idx)
   
   