function o= sum_entropy (p)
    sum=0;
    
    for k=2:2*256
        
        p_sum=0;
        
        if k > 256+1
            for i= (k-256): 256
                p_sum =p_sum + p(i,(k-i) ) ;
            end
        else   
            for i=1:k-1
                p_sum =p_sum + p(i, (k-i)) ;
            end
        end
        
        sum =sum + - log(p_sum+1) * p_sum;
    end
    
    o= sum ;

end