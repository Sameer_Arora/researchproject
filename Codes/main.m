
%burn_eval('images','output', 'haraff');

img= imread('images/test.png') ;

img1=double(img);

L= zeros(size(img,1),size(img,2),size(img,3)); 
Retinex = zeros(size(img,1),size(img,2),size(img,3)); 

figure(10)

for i=1:size(img,3)
  L(:,:,i) =  log( 1+ img1(:,:,i)  ) * 1/log( 256 ) ;
  
  Retinex(:,:,i) = retinex_frankle_mccann(L(:,:,i) , 19); 
    

end

% Display the original image
subplot(1, 3, 1);
imagesc(img); 
title('Original');

% Display the original image 
subplot(1, 3, 3);
imagesc(Retinex); 
title('Retinex');

% Display compressed image side by side
subplot(1, 3, 2);
imagesc(L)
title('Log transformed .');

%Error= abs(L-Retinex);

figure(2)
% Display compressed image side by side
subplot(1, 2, 1);
imshow(Retinex )
title('My out .');

img_1= imread('images/resmc.png') ;

% Display compressed image side by side
subplot(1, 2, 2);
imshow(img_1 )
title('out .');


% eval_des('datasets','output/hesaff', 'hesaff');
% eval_des('datasets','output/mser', 'mser');
% eval_des('datasets','output/ibr', 'ibr');
% eval_des('datasets','output/ebr', 'ebr');