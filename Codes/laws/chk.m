clc
close all
set(0,'DefaultFigureWindowStyle','docked');

for id=1:5
    
    if(id==6)
        continue;
    end
    
    Image = imread ( ['../images/raw_images/r' num2str(id) '.jpg'] );
    
   %% Apply various color constancy algorithms
     figure(10);


     % Display color corrected image side by side
    subplot(1, 2, 1);
    imagesc ( Image )
    title('org ');

    Image= colorConstancy(Image, 'modified white patch', 200);

     % Display color corrected image side by side
    subplot(1, 2, 2);
    imagesc ( Image )
    title('color ');
    
    %%
    imLAB = rgb2lab(Image);
    smoothedLAB = imbilatfilt(imLAB);
    
    smoothedRBG = lab2rgb(smoothedLAB,'Out','uint8');
    
    eroor= abs(smoothedRBG-Image);
    
    figure;
    montage({Image,smoothedRBG,eroor})
    title(['Original Image vs. Filtered Image  '])
    
   

        
%     figure;
%     subplot(131);
%     imshow(Image);
%     impixelinfo;
%     title('Original Grey Image');
% 
%     subplot(132);
%     imshow(img);
%     impixelinfo;
%     title('Filtered Grey Image');
% 
%     subplot(133);
%     imshow(abs(img-Image) );
%     impixelinfo;
%     title('Eror Grey Image');

    [mapz] = laws( Image , 3); %3 is serendipitously choose
    
    for i=1:9
        figure;
        imshow(mapz{1,i});
    end
    
end