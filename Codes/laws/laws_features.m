function laws_feat= laws_features(Image)
% clc
% close all
% 
% %% Apply various color constancy algorithms
%  figure(10);
% 
% 
%  % Display color corrected image side by side
% subplot(1, 2, 1);
% imagesc ( Image )
% title('org ');
% 
% Image= colorConstancy(Image, 'modified white patch', 200);
% 
%  % Display color corrected image side by side
% subplot(1, 2, 2);
% imagesc ( Image )
% title('color ');
% 

%% filtering out noise from imges.
% 
% imLAB = rgb2lab(Image);
% smoothedLAB = imbilatfilt(imLAB);
% 
% smoothedRBG = lab2rgb(smoothedLAB,'Out','uint8');
% 
% eroor= abs(smoothedRBG-Image);
% 
% figure;
% montage({Image,smoothedRBG,eroor})
% title(['Original Image vs. Filtered Image  '])
%     figure;
%     subplot(131);
%     imshow(Image);
%     impixelinfo;
%     title('Original Grey Image');
% 
%     subplot(132);
%     imshow(img);
%     impixelinfo;
%     title('Filtered Grey Image');
% 
%     subplot(133);
%     imshow(abs(img-Image) );
%     impixelinfo;
%     title('Eror Grey Image');

[m n ~] = size(Image);

[mapz] = laws( Image , 3); %3 is serendipitously choose


laws_img= zeros(m,n);

ap=cell2mat(mapz(1,1) );

for i=1:9
    laws_img= laws_img + double( cell2mat(mapz(1,i) ) );
end

laws_img= laws_img / 9 ;
laws_feat= color_feat( laws_img ) ;

end
