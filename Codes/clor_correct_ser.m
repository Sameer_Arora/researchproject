%%  Adding various subfolders .

%set(0,'Default%figureWindowStyle','docked');
% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 

% Add that folder plus all subfolders to the path.
addpath(genpath(folder));
clc;
close all;

% file to store results.

fid = fopen('results.dat','w');
fprintf(fid,' Filename clolrspace channel clustering F1 Dice Jacc \n');

% folder ot store %figures.
figures='figures/';

if exist(figures, 'dir') == 0
        mkdir(figures);
end

if exist('output/fal_neg_patch/', 'dir') == 0
        mkdir('output/fal_neg_patch/');
end

if exist('output/fal_pos_patch/', 'dir') == 0
        mkdir('output/fal_pos_patch/');
end

%folder store the truth matrices
gt_truth= 'gt_truth/';

if exist(gt_truth, 'dir') == 0
        mkdir(gt_truth);
end

% stroting the patches.
patches=[];
patches_test=[];


% training data for ML;

Y_train=[];
X_train=[];

Y_test=[];
X_test=[];

% flag for test images.
test=0;

for id=1:20
    
    if id==6
        continue;
    end
    
    name=['r' num2str(id) '.jpg'];
    scl_down=3;
        
    % indexs to remove
    if( nnz( id== [11 14:20] ) >  0 )
        test=1;
    end
    
    
    %img= imread([ 'images/raw_images/' name ]); 
    %pause;
    
    % saving the segemented images.
    %imwrite(maskedImage, [ 'images/nb_images/' num2str(id) '_nb.jpg' ]);
    
 
    mar_img= imread( [ 'images/markedimages/F_' num2str(id) '.jpg' ]) ;
    if test==1
        mar_img = imresize(mar_img,1/scl_down); 
    end
    
    %img= imread([ 'images/nb_images/' num2str(id) '_nb.jpg' ]); 
    img= imread([ 'images/raw_images/' name ]); 
    if test==1
        img = imresize(img,1/scl_down); 
    end
    % making the images of same sizes.
    if( nnz(size( img ) ~= size(mar_img) ) >0 )
        row=min(size(img,1) ,size(mar_img,1) );
        col=min(size(img,2) ,size(mar_img,2) );
        
        img= img(1:row ,1:col,:);
        mar_img= mar_img(1:row ,1:col,:);
    end
        
    
    img1=double(mar_img);
    
    %%%%imagesc(img);
    % saving results in csv fils.
    
    
   %% Generating ground truth
    [m, n, ch]=size(mar_img);
    

    %markColor = double( img(4,4,:) );

    fillColor = [38 230 0] ;
    fillColor = reshape(fillColor,1,1,3);

    markColor = [0 77 229 ] ;
    markColor = reshape(markColor,1,1,3);

    Locations=[];
    X=[];
%     
%     for i=1:m
%         for j=1:n
%             X= [ X ; sum( (img1(i,j,:) - fillColor).^2 ) ];
%             if sum( (img1(i,j,:) - fillColor).^2 ) <8
%                 Locations= [ Locations ; [i j] ];
%             end
%         end
%     end

    temp= repmat (fillColor ,m,n ,1);

    X= sum( (img1 - temp).^2,3);

%     %figure(90);
%     %%%imagesc(X);
%     %figure(98);
%     
%    in1=( X<80 );

    in2=find( X< 300 );

    Locations= [ ceil(in2/m) rem(in2,m)+1 ] ;
    Locations= reshape(Locations, size(in2,2)* size(in2,1) ,2 );


    % Plot the data
%     %figure(19);
%     %%%imagesc(img);
%     %impixelinfo;
%     hold on;
%     plot(Locations(:,1), Locations(:,2), 'r*', 'LineWidth', 2, 'MarkerSize', 5);
% 

        % chechking if grtruth already exixts.
    if( exist([ gt_truth name '.mat'],'file' ) ~= 2  )
    
        temp= repmat ( markColor ,m,n );

        %temp=reshape(temp,m,n,ch);

        X= sum(( img1 - temp).^2 ,3);

        in1=( X < 9000 );
        %figH= figure;

        %figure(9);
        %%%imagesc(mar_img);
        %impixelinfo;
        %title('original Image');

        %plot(Locations(:,1), Locations(:,2), 'r*', 'LineWidth', 2, 'MarkerSize', 5);

        %close all;
        
        %figure(10);

        binary= double(rgb2gray(mar_img)) .* in1 ;
        %%%imagesc(binary);
        %imsave
        %subplot(2,2,1)
        %%%imagesc(binary);

        %title('Binary Image');

        %subplot(222);

        %%%imagesc(mar_img);



    %% Apply edge linking and thinning .

        %subplot(223);
        

        bw2 = filledgegaps(binary, 7 );
        %%%imagesc(bw2);


        index=find( bw2 == 1 );

        img2= reshape(mar_img,m*n,ch);

        
        img2( index,:)= repmat( reshape( markColor,1,3 ) , size(index,1),1 ) ;

        mar_img=reshape(img2,m,n,ch);


        %subplot(224);
        %%%imagesc(mar_img);
        %impixelinfo;

        %title('Edge joined  Image');


    %% getting the marked area.
       %change it
        %close all;
        %Locations=[165 110 ];

        [ img_marked, ms ]= bfs_ser( Locations,mar_img,fillColor,markColor );

        gr_truth= zeros(m,n);
        gr_truth(ms)=1 ;

        save( [gt_truth name '.mat' ],'gr_truth' );

        %figure(100);
        %subplot(1,2,1);
        %%%imagesc(mar_img);
        %impixelinfo;

        %subplot(1,2,2);
        %%%imagesc(img_marked);
        %impixelinfo;
        %title('flood fill');

    else
        example = matfile([gt_truth name '.mat']);
        gr_truth = example.gr_truth;
    end
    
    %% Gentrating train sets and test sets for images
    [m, n, ch]=size(img);
    patch_dim=5;
    patch_size=64;
        
   % ca = patch_ser(img, floor(n/patch_dim) , floor(m/patch_dim) );
    ca = patch_ser(img, patch_size , patch_size,0 );
    
    
    ca=reshape(ca,size(ca,1)*size(ca,2),1 );
    
    
    % adding texture and color features in images
    for i=1:size(ca,1)
        
        im_patch=cell2mat(ca(i));
        o=texture_feat( im_patch ) ;
        
        co=color_feat( im_patch ) ; 
        if  test==0
            patches=[ patches;  im_patch ];
            X_train= [X_train ; co o ] ;
        else
            patches_test=[ patches_test ;  im_patch ];
            X_test= [X_test ; co o ] ;
        end    
    end
    
    %ca = patch_ser(gr_truth, floor(n/patch_dim) , floor(m/patch_dim) );
    ca = patch_ser(gr_truth, patch_size , patch_size,0);
    
    
    %im_patch_size= floor(n/patch_dim) * floor(m/patch_dim)  ;
    im_patch_size= patch_size * patch_size ;
    
    ca=reshape(ca,size(ca,1)*size(ca,2),1 );
    
    
    % adding texture and color features in images
    for i=1:size(ca,1)
        
          im_patch=cell2mat(ca(i));
%         no_zero=nnz(im_patch);
%         siz=im_patch_size/2 ;
%       
        if test==0
            if nnz(im_patch) > (im_patch_size/2)
                Y_train= [Y_train ; 1]  ;
            else 
                Y_train= [Y_train ; 0]  ;
            end
        else
            if nnz(im_patch) > (im_patch_size/2)
                Y_test= [Y_test ; 1]  ;
            else 
                Y_test= [Y_test ; 0]  ;
            end
        end
    end
    
    %saving the matrices.
    %save('X_train','X_train');
    %save('Y_train','Y_train');
          
%     %% Apply various color constancy algorithms
%      %figure(10);
%    
%     
%      % Display color corrected image side by side
%     %subplot(1, 2, 1);
%     %%%imagesc ( img )
%     %title('org ');
%     
%     img= colorConstancy(img, 'modified white patch', 200);
%     
%      % Display color corrected image side by side
%     %subplot(1, 2, 2);
%     %%%imagesc ( img )
%     %title('color ');
%     
%     %% Check various color spaces.
%     img1=double(img);
%     
%     for ty=1:3
%         
%         close all;
%         [ img_trans tname]= colorspaceViualize(img,ty,name) ;
%         %mean= computeMean(img_trans);
%         
%         
%    
%         %% applying fuzzy cmeeans for all the channels nochannels, img,noiteratons .
%         [ out_img out_img2 clust]= fuzzycmeanssegmentation ( 3 , img ,100 , Locations(1,:)  ) ;
%         
%         figH = %figure;
%         set(figH,'Name','Fuzzy C Segemetation result','Number%title','off') ;
%         
%         
%         % Display compressed image side by side
%         %subplot(2, 3, 1);
%         %%%imagesc ( img );
%         %title('Org');
% 
%         for j=1:3
%             % Display compressed image side by side
%             %subplot(2, 3, j+1);
%             %%%imagesc ( out_img(:,:,j) );
%             %impixelinfo;
%             %title( ['Fuzzy C means out ch ' num2str(j) ] );
%             
%             [acc pre rec jac dic F1]=evalu( out_img(:,:,j) , gr_truth ) ;
%             fprintf(fid,'%s %d %d fuzzyc %f %f %f \n',name,id,ty,j,F1,dic,jac);
%             
%             
%         end
% 
%         % Display compressed image side by side
%         %subplot(2, 3, 5 );
%         %%%imagesc ( out_img2 )
%         %impixelinfo;
%         %title( ['Fuzzy C means out all ch '] );
%         
%         saveas(figH, [%figures name tname '_fuz.png '] );
%         
%         [acc pre rec jac dic F1]=evalu( out_img2 , gr_truth );
%          
%         fprintf(fid,'F%d.jpg %d %d fuzzyc %f %f %f \n',id,ty,4,F1,dic,jac);
%          
%         
%         %% applying spectral clustering for all the channels nochannels, img .
%         %out_img1=spectralsegmentaion(2,img);
%         
%         figH = %figure;
%         set(figH,'Name','Spectral Segemetation output','Number%title','off') ;
%          
%         % Display compressed image side by side
%         %subplot(2, 3, 1);
%         %%%imagesc ( img );
%         %title('Org');
%         
%         for j=1:3
%             
%             % parameters  ( img,noclusters,no_neighbours)
%             out_img = spectralsegmentaion(img(:,:,j) ,4 ,Locations(1,:) );
% 
%             % Display compressed image side by side
%             %subplot(2, 3, j+1);
%             %%%imagesc ( out_img );
%             %impixelinfo;
%             
%             %title( [' Spec C  out ch ' num2str(j) ] );
%             
%             [acc pre rec jac dic F1]=evalu( out_img , gr_truth ) ;
%             
%             fprintf(fid,'F%d.jpg %d %d spectral %f %f %f \n',id,ty,j,F1,dic,jac);
%             
%             
%        end
%         
%         % parameters  ( img,noclusters,no_neighbours)
%         out_img = spectralsegmentaion(img ,4 ,Locations(1,:) );
%        
%         % Display compressed image side by side
%         %subplot(2, 3, 5);
%         %%%imagesc ( out_img );
%         %title('Spec C  out ch 4');
%        
%         saveas(figH, [%figures name tname '_spec.png '] );
%         
%         [acc pre rec jac dic F1]=evalu( out_img , gr_truth );
%          
%         fprintf(fid,'F%d.jpg %d %d spectra %f %f %f \n',id,ty,4,F1,dic,jac);
%         
%         
%     end
%     
    
    
    
%     plot(centers(1,1),centers(1,2),'xb','MarkerSize',15,'LineWidth',3)
%     plot(centers(2,1),centers(2,2),'xr','MarkerSize',15,'LineWidth',3)
%     hold off
    
end

%% saving the matrices. training the model and checking results.
save('X_test','X_test');
save('Y_test','Y_test');
save('X_train','X_train');
save('Y_train','Y_train');

z=[X_train Y_train];    

z1=[X_test Y_test];

[trainedClassifier, validationAccuracy,validationPredictions,validationScores] = trainClassifier_svm_gauss(z);

% selecting false postives 
fp= find (validationPredictions & ~Y_train ==1 )

for i=1:size(fp,1)
    
    %imshow( patches ( (fp(i)-1)*64:fp(i)*64,:,: ) ) ;
    imwrite( patches ( (fp(i)-1)*64:fp(i)*64,:,: )  ,[ 'output/fal_pos_patch/pat' num2str(i) '.jpg'] ) ;
end

%figure;
% selecting the false negatives.
fn=find ( ~validationPredictions & Y_train ==1 )
for i=1:size(fn,1)
    %imshow( patches ( (fn(i)-1)*64:fn(i)*64,:,: ) ) ;
    imwrite( patches ( (fn(i)-1)*64:fn(i)*64,:,: )  ,[ 'output/fal_neg_patch/pat' num2str(i) '.jpg'] ) ;
end

% testing the model on test sets.
pred =trainedClassifier.predictFcn(X_test);

[acc pre rec jac dic F1]=evalu( pred,Y_test ) 

for i=1:size(fp,1)
    
    %imshow( patches ( (fp(i)-1)*64:fp(i)*64,:,: ) ) ;
    imwrite( patches ( (fp(i)-1)*64:fp(i)*64,:,: )  ,[ 'output/fal_pos_patch/pat' num2str(i) '.jpg'] ) ;
end

%figure;
% selecting the false negatives.
fn=find ( ~validationPredictions & Y_train ==1 )
for i=1:size(fn,1)
    %imshow( patches ( (fn(i)-1)*64:fn(i)*64,:,: ) ) ;
    imwrite( patches ( (fn(i)-1)*64:fn(i)*64,:,: )  ,[ 'output/fal_neg_patch/pat' num2str(i) '.jpg'] ) ;
end

