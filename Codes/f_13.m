function o = f_13( p,f_9 )

        px= sum(p , 1) ;
        py= sum(p , 2);
        
      
        HXY2= - sum(sum(px'*py' .* log( px'*py' +1) ));
       
        o= ( 1- exp(-2* abs(HXY2-f_9) ) ).^(1/2);
        
end