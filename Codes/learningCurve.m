function [error_train, error_test,figH] = ...
    learningCurve(X, y, Xtest, ytest, train_func )
%LEARNINGCURVE Generates the train and cross validation set errors needed 
%to plot a learning curve
%   [error_train, error_val] = ...
%       LEARNINGCURVE(X, y, Xval, yval, lambda) returns the train and
%       cross validation set errors for a learning curve. In particular, 
%       it returns two vectors of the same length - error_train and 
%       error_val. Then, error_train(i) contains the training error for
%       i examples (and similarly for error_val(i)).
%
%   In this function, you will compute the train and test errors for
%   dataset sizes from 1 up to m. In practice, when working with larger
%   datasets, you might want to do this in larger intervals.
%

% Number of training examples
m = size(X, 1);

m_test= size(Xtest, 1); 

% You need to return these values correctly
error_train = zeros(min(m,m_test), 1);
error_test   = zeros(min(m,m_test) , 1);

min(m,m_test)
for i = 5: 5 :min(m,m_test)
  z=  [X(1:i, :)  y(1:i) ];
  [~,~,~,~,error_train(i)] = train_func( z ,3);

  z=  [Xtest(1:i, :)  ytest(1:i) ];
  [ ~,~,~,~, error_test(i)] = train_func( z ,3);

  %  and y(1:i), storing the result in 
  % error_train(i) and error_val(i)
  ....

end

% saving the learning curve.

    figH = figure;
    set(figH,'Name','Learning Curves','NumberTitle','off') ;

    plot( 5:5:size(X_test,1), error_train, 5:5:size(X_test,1) , error_val);
    title('Learning curve for linear regression')
    legend('Train', 'Test ')
    xlabel('Number of training examples')
    ylabel('Error')
    %axis([0 13 0 150])
  
    saveas(figH, [ modelname num2str((2^patch_factor )*8) 'output/learningcurve.png'] );
  
end
