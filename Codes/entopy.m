function o= entopy (p)
    
    o= sum(sum( - p.* log(p+1) )) ;

end