function o = auto_correlation( p )

        px= sum(p , 1) ;
        py= sum(p , 2);
        
        I=[0:255]; 
        
        ux=  I * px' ;
        uy=  I * py ;
        
        var_x= I.^2 * px' - ux.^2 ;
        var_y= I.^2 * py  - uy.^2 ;
        
        
        sig_x = sqrt(var_x);
        sig_y = sqrt(var_y);
        
        p_sum=0;
        
        te= I'* I ;
    
         mu= sum( sum( te .* p ) );

        
        o = ( mu - (ux)*(uy) ) / (sig_x* sig_y)  ;
        
end