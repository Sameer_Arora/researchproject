function [img_trans,tname]=colorspaceViualize(img,Type,name)     

%  Type 1= Ycbcr
%  Type 2= Hsv
%  Type 3= L*U*V


if ~any(Type == (1:4))
   ME = MException('InvalidCall:UnknownType', ...
       'Unknown Color Space');
   throw(ME);
end

if Type==1 
    img_trans = double(rgb2ycbcr(img));
    figH = figure;
    tname='Ycbcr';
    set(figH,'Name',tname,'NumberTitle','off')
    
elseif Type==2
    img_trans = double(rgb2hsv(img));
    figH = figure;
    tname='HsV sapce';
    set(figH,'Name',tname,'NumberTitle','off')
    
elseif Type==3 
    figH = figure;
    tname='LUV';
    set(figH,'Name',tname,'NumberTitle','off')
    
    img1=double(img);
    cform = makecform('srgb2xyz');
    xyz_img = applycform(img1,cform);
    cform = makecform('xyz2uvl');
    img_trans = applycform(img1,cform);

elseif Type==4
    img_trans = double(rgb2lab(img));
    figH = figure;
    tname='Lab';
    set(figH,'Name',tname,'NumberTitle','off')
    
end          



%% Visuliaze 

    

    % Display the original image
    subplot(2, 2, 1);
    imagesc(img); 
    title('Original');

    % Display the original image 
    subplot(2, 2, 2);
    imagesc( img_trans(:,:,1) ); 
    title('ch 1');

    % Display compressed image side by side
    subplot(2, 2, 3);
    imagesc ( img_trans(:,:,2) )
    title(' Ch 2');

    % Display compressed image side by side
    subplot(2, 2, 4);
    imagesc ( img_trans(:,:,3) )
    title('Ch 3');

    %saveas(figH, [name '_' tname '.png '] );
    
end