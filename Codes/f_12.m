function o = f_12( p,f_9 )

        px= sum(p , 1) ;
        py= sum(p , 2);
        
        
        HXY1= - sum(sum(p .* log( px'*py' +1) ));
       
        o  = ( f_9 - HXY1 ) / max( f_9 , HXY1 );
        
end