
% function to retiurn the basic statistical propreties present in the patch
% for color.

function o = color_feat( img )
   
    [m, n, ch ]=size(img);
    o=[];
    
    for i=1:ch
    
            img1=double( img(:,:,i) );


            mn= sum( sum(img1,1) ) / (m*n)  ;

            var = sum( sum(img1.^2 ,1 ) ) / (m*n)  - mn.^2 ;

            std= var.^(1/2) ;

             int_= reshape(img1, m*n ,1);

            [ ~ ,min_] = min( int_ ) ;
            [~, max_] = max( int_ ) ;

            if  rem(min_,m) ==0
                remd = m ;
            else
                remd = rem(min_,m);

            end

            min_= img1( remd , ceil(min_/m) );

            if  rem(max_,m) ==0
                remd = m ;
            else
                remd = rem(max_,m) ;

            end


            max_= img1(  remd , ceil(max_/m) );
            
            med= median(int_);
            
            o=[ o med mn std min_ max_ ];
    
    end    
    
end