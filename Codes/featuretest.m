clc;
clear all;

Data=[];

% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 

% Add that folder plus all subfolders to the path.
addpath(genpath(folder));


%%
for j=3:3
    name=[ num2str(j) '_nb'];
    img=imread([ 'images/no background image/' name '.png'] );
    [m n ~]= size(img);
    
    ca = patch_(img, floor(n/3) , floor(m/3) );
    
    ca=reshape(ca,size(ca,1)*size(ca,2),1 );
    

    for i=1:size(ca,1)
        im_patch=cell2mat(ca(i));
        
        o=texture_feat( im_patch ) ;
        Data= [Data ;o] ;
    end
end


%%
Red = Pca( Data ,2 );
figH= figure;
set(figH,'Name','texture features','NumberTitle','off') ;
        
scatter3( Red(:,1 ) ,Red(:,2 ) , [1:size(ca,1)]' ) ;

% Add labels and title
xlabel('x')
ylabel('y')
zlabel('pathchno')
title('Texturre features');
