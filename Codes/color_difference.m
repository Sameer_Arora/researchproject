function X= color_difference(img,markColor)  

    [m n ch]=size(img);
    
    markColor= reshape( markColor ,1,1,3);
    temp= repmat ( markColor ,m,n );
    
    %temp=reshape(temp,m,n,ch);
    img1=double(img);
    
    X= sum( (img1 - temp).^2 ,3);
    
    
end
    