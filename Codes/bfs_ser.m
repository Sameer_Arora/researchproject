function  [img_mar Ms] = bfs( Loc,img,fillColor ,markColor)
    

[m n ch]=size(img);

img_mar =img;

Ms=[0];

% convention X is col and Y is row .

%Intialize all image points to zero
visited=zeros(m,n);

% figure(1)
% imagesc(img);
% hold on;

% figH = figure;
% set(figH,'Name','ground truth generate','NumberTitle','off') ;
%         

for i=1:size(Loc,1)
    
% IMplemented BFS running slow

%     import java.util.LinkedList
%     q = LinkedList();
%     pt=Loc(i,:);
% 
%     if ( visited( pt(1),pt(2) ) ==1 )
%         continue;
%     end
%     
%     q.add( Loc(i,:) );
%     
%     
%     visited( pt(1),pt(2) ) =1;
%     
%     img_mar ( pt(1),pt(2) ,: )= fillColor;
%      
%     while( q.size()~=0 ) 
%     
%         pt =  q.getFirst() ;
%         x=pt(1);
%         y=pt(2);
%         
%         for l= -1:1
%                for k=-1:1
% %                 visited(x+l ,y+k ) ~=1
% %                 sum( (img(x+l,y+k,:) - markColor).^2 ) > 80
% %                 
%                 %plot(x+l,y+k, 'r*', 'LineWidth', 2, 'MarkerSize', 2) ; 
%                 
%                 if( visited(x+l ,y+k ) ~=1 && sum( (img(x+l,y+k,:) - markColor).^2 ) > 80 )
%                     q.add( [ x+l ,y+k  ] );
%                     visited( x+l ,y+k  ) =1; 
%                     img_mar ( x+l,y+k,: )= fillColor;
%                 end
%                 
%             end
%         end
%         
%         
%         
%     end

%img= imread('images/7.png');

%img = repmat(img,1,1,3);
% 
% subplot(1,2,1);
% imagesc(img);
% impixelinfo;

   
% title('original image');


tol=80; % tolerance
r=Loc(i,:); % start point of flood

%ms=flood_fill(img,r,tol,markColor); % make flood fill


% avoiding repetetive calling for filled locations.

if visited( r(2),r(1) ) ~= 1
    ms=flood_fill(img,r,tol,markColor); % make flood fill
    
    % adding the locations to list Ms
    
    Ms=[Ms ;ms];
    
    visited(ms)=1;

end


end

Ms=Ms(2:end);

cl=[0; 255; 255];% color to fill

R=img(:,:,1);
G=img(:,:,2);
B=img(:,:,3);

R(Ms)=cl(1);
G(Ms)=cl(2);
B(Ms)=cl(3);

img_mar(:,:,1)= R;
img_mar(:,:,2)=G;
img_mar(:,:,3)=B;

% subplot(1,2,2);
% imagesc(img_mar);
% impixelinfo;
% title('flood fill');



end