function v = ToVector_m(im)
sz = size(im);
v = reshape(im, [ prod(sz(1:2)) sz(3) ]);
[~,laws_feat]=laws_features_point(im);
v= [ v  laws_feat] ;

end