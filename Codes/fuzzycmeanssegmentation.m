function  [out_img out_img2 clust] = fuzzycmeanssegmentation (no_clusters, img,no_iteration , cluster_pt ) 
    
    refresh;
    figH = figure;
    set(figH,'Name','Fuzzy Segemetation','NumberTitle','off')
   
    % which clusters is the wound being assigned. 
   
    clust=zeros(4,1); 
   
   % Display compressed image side by side
    subplot(2, 3,1 );
    imagesc ( img )
    title('orig');
    
    [s1,s2,ch]=size(img);
    test_img=zeros(s1,s2,ch);
    out_img=zeros(s1,s2,ch);
    
    img=double(img);
    
    for j=1:ch
        
        img_gray= img(:,:,j);
        
        fcmdata= img_gray;
        
        fcmdata=reshape(fcmdata,size(img_gray,1)*size(img_gray,2),1) ;
        [~,laws_feat]= laws_features_point (img_gray);
        
        fcmdata= [fcmdata laws_feat];
        
        [centers,U] = fcm(fcmdata ,no_clusters,[2 no_iteration]);

        % write a function for this.
        idx = findClosestCentroids(fcmdata,U);

        clust(j) = idx( tn( cluster_pt(1), cluster_pt(2),s1 ) ) ;
        
        temp= centers( idx ,1 ) ;

        test_img(:,:,j)=reshape(temp,s1,s2) ;
        
        
        % Display compressed image side by side
        subplot(2, 3, j+1);
        imagesc ( test_img(:,:,j) )
        title( ['Fuzzy C means out ch ' num2str(j) ] );
        
        
        % changing the cneters for clusters
        centers= centers .* 0; 
        centers( clust(j),: )=  ones( 1, size(centers,2) );
        
        temp= centers( idx ,1 ) ;

        out_img(:,:,j)=reshape(temp,s1,s2) ;
        
  
    end
    
    fcmdata=img;
    [~,laws_feat]= laws_features_point (img_gray);
    fcmdata=reshape(fcmdata,size(img_gray,1)*size(img_gray,2),3) ;

    fcmdata= [fcmdata laws_feat];

    
    [centers,U] = fcm(fcmdata ,no_clusters,[2 no_iteration]);
    
    centers =centers(:,1:3);
    % write a function for this.
    idx = findClosestCentroids(fcmdata,U);
    
    clust(4) = idx( tn( cluster_pt(1), cluster_pt(2),s1 ) ) ;
        

    temp= centers( idx ,: );

    test_img2 = uint8(reshape(temp,s1,s2,3)) ;
    
    % Display compressed image side by side
    subplot(2, 3, j+2);
    imagesc ( test_img2 );
    impixelinfo;
    title( ['Fuzzy C means out all ch '  ] );
    
    temp = ( idx == clust(j) ) ;

    out_img2=reshape(temp,s1,s2) ;
% 
%     % Display compressed image side by side
%     subplot(2, 3, j+3);
%     imagesc ( uint8(test_img ));
%     impixelinfo;
%     title( ['Fuzzy C means out all ch '  ] );

        
end