function Locations=gen_ground_truth(fillColor,markColor,img1)
        [m n ~]=size(img1);
        temp= repmat (fillColor ,m,n ,1);

        X= sum( (img1 - temp).^2,3);

        in2=find( X< 300 );

        Locations= [ ceil(in2/m) rem(in2,m)+1 ] ;
        Locations= reshape(Locations, size(in2,2)* size(in2,1) ,2 );


        % Plot the data locations 
    %     figure(19);
    %     imagesc(img);
    %     impixelinfo;
    %     hold on;
    %     plot(Locations(:,1), Locations(:,2), 'r*', 'LineWidth', 2, 'MarkerSize', 5);
    % 

end