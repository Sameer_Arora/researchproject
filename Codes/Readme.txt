####################################
Author:- Sameer
Entry Number:- 2016csb1058
Subject:- Summer Project Burn detection and classification.
Date:- 2/2/18 
####################################


####################################
How to run and build ?
####################################

I have provided the folder containing all the .m used in my program .

The main file and thier usage:-

1)main.m:- file to drive the entire segmentation and classification process.


2) clor_correct.m:- it is the main file which drives all the steps till segmentation of the image.

3) test*.m:- are temporary file made for trying out various different approaches.

4) fuzzycmeanssegmentation.m:- it is the file which does the segmentation in images using the inbuilt fcm algorithm.

5) burn_eval.m:- this file deals with the integragtion of the segmentation and classification approaches used.

Rest all files are just supportive and can be found from these main files used in project.


####################################
Working
####################################
In this program i have implemented a predictor system which generates the facts as given in the yaml file and based on facts formulate some rules that are used to query the knowledge base.

####################################
Description
####################################



####################################
Sample Run 
####################################

sample output given in design documentation
