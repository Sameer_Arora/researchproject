function [ acc preci recal jac_ind dic_coeff F1] = evalu( res , gr_truth )
    

    % nnz function gives numver of non zeros elements in matrix;
    tp= nnz(res .* gr_truth );
    fp= nnz( res.* (~ gr_truth) );
    
    tn= nnz( ~res .* ~ gr_truth );
    fn= nnz(~res .* gr_truth ) ;
    
    acc=  (tp+tn )/ (tp+tn+fn+fp );
    
    preci= tp / (tp +  fp);
    recal= tp/( tp + fn );
    
    jac_ind= tp / (tp+fp+fn);
    
    dic_coeff= 2* tp /( 2*tp +fp+ fn);
    
    F1 = 2* preci * recal /(preci+recal);
    

end