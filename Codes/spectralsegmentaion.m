function [S1]= spectralsegmentation(Img,k,cluster_pt, Neighbors ,roundColors,roundDigits,saveData, markEdges)

%Parameters for spectral clustering 

roundColors = 0;        % Round color values for less strict uniqueness
roundDigits = 2;        % Precision for Uniqueness
saveData    = 0;        % Save Dataset
markEdges   = 0;        % Outline edges



% =============

[m, n, d] = size(Img);

%S1=zeros(m,n);

Neighbors= ceil(log(m*n)) ;

% convert into list of data points
Data = reshape(Img, 1, m * n , []);

if d >= 2
    Data = (squeeze(Data))';
end

% convert to double and normalize to [0,1]
Data = double(Data);
Data = normalizeData(Data);

if isequal(saveData, 1)
    [savePath, saveFile, ~] = fileparts(FileName);
    
    csvwrite(fullfile(relativepath(savePath), ...
        [saveFile '.nld']), Data');
end

% Find unique colors
if isequal(roundColors, 1)
    fac = 10^roundDigits;
    rData = round(Data * fac) / fac;
else
    rData = Data;
end

[~, ind, order] = unique(rData', 'rows', 'R2012a');

% crop data keeping the unique values.

Data = Data(:, ind);

% now for the clustering

fprintf('Creating Similarity Graph...\n');
SimGraph = SimGraph_NearestNeighbors(Data, Neighbors, 1);


comps = graphconncomp(SimGraph, 'Directed', false);
fprintf('- %d connected components found\n', comps);



fprintf('Clustering Data...\n');
C = SpectralClustering(SimGraph, k, 3);


% convert and restore full size
D = convertClusterVector(C);
D = D(order);

% reshape indicator vector into m-by-n
S = reshape(D, m, n);

clust = S ( cluster_pt(1), cluster_pt(2) )  ;
S1 = ( S==clust ); 

        

% choose colormap
if k == 2
    map = [0 0 0; 1 1 1];
else
    map = zeros(3, k);
    
    for ii = 1:k
        ind = find(D == ii, 1);
        map(:, ii) = rData(:, ind);
    end
    
    map = map';
end

% 
% figH = figure;
% set(figH,'Name','Spectral Segemetation ','NumberTitle','off') ;
%         
% if isequal(markEdges, 1)
%     imshow(Img, 'Border', 'tight');
%     
%     lS = label2rgb(S);
%     BW = im2bw(lS, graythresh(lS));
%     [B, ~] = bwboundaries(BW, 'holes');
% 
%     hold on;
%     
%     for k = 1:length(B)
%         boundary = B{k};
%         plot(boundary(:, 2), boundary(:, 1), 'r', 'LineWidth', 2)
%     end
%     
%     hold off;
% else
%     imshow(S, map, 'Border', 'tight');
%     
%     imshow(S1, map, 'Border', 'tight');
%     impixelinfo;
% end
% 
% hold on;
% 
% axis off;
% %truesize;
% hold off;

end