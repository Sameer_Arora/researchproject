function o= variance (p)
    

    I=[0:255]; 
    
    te= I'* I ;
    
    mu= sum( sum( te .* p ) );
    
    te = (te -mu).^2 ;
    
    o= sum(sum( te.* p )) ;

end