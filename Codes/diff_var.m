function c= diff_var (p )
    var=0;
    mean=0;
    for k=1:256-1
                p_var=0;
                
                
                for i1= 1: 256-k
                    p_var= p_var + 2 * p(i1,i1+ k)  ;
                end
                p_var= p_var * k.^2;
                mean = mean + p_var * k ;
                
                var= var+p_var;
    end

    
    c= (var -mean.^2 );
end