function o= inv_diff_moment (p)
    

    I=[0:255]; 
    
    m_ii=  repmat (I.^2' ,1,256) ;
    m_jj= repmat ( I.^2 , 256,1 );
    
    m_ij = I'* I ;
    
    m_i_j = m_ii+ m_jj -2 * m_ij ;
    
    
    te= 1 ./ ( 1 + m_i_j ) ;
    
    o= sum(sum( te.* p )) ;

end