% *************************************************************************
% Evaluate the performance of different descriptors on the affine covariant
% regions
%
% Input:
% input_dir - the datasets directory
% output_dir - the result directory
% detector - the name of the used affine covariant detetor, which is the
% expanded-name of the output region file
%

function burn_eval(input_dir, output_dir, detector)
t0 = clock;

%match strategy
% MATCH_NN = 201;
% MATCH_SIM = 202;
MATCH_RN = 203;


% All the case
% sub_dirs = {'bikes', 'boat', 'graf', 'leuven', 'ubc', 'wall', 'desktop', 'corridor', 'desktop_syn'};
% file_ext = {'.ppm',  '.pgm', '.ppm', '.ppm',   '.ppm','.ppm', '.ppm',    '.ppm',      '.ppm'};
% img_begin = [1 1 1 1 1 1 1 1 1];
% img_end =   [6 6 6 6 6 6 5 6 3];
% H_postfix = {'p','p','p','p','p','p','p','p','p'};

% Example case
sub_dirs = {'images'};
file_ext = {'.jpg'};
img_begin = 1;
img_end =   20;

color_sty = {'b','g','m','c','k', 'y'};
marker_sty = {'*', '^', 'o', 's', 'd', 'p', '+', '>', '<'};
%line_sty = {'-', '--', '-.', ':', '-'};


for i=1:size(sub_dirs, 2)
    
    %input_path = [input_dir '/' sub_dirs{i} '/'];
    %output_path = [output_dir '/' sub_dirs{i} '/'];
   
    input_path = [input_dir '/' ];
    output_path = [output_dir '/'];
    ext = file_ext{i};
    
    if exist(output_path, 'dir') == 0
        mkdir(output_path);
    end
    
    refstr = num2str(img_begin(i));
    ref_img_file = [input_path 'b' refstr ext];

    %% viusualizing the images in dataset
    
    img1 = imread(ref_img_file);
    figure(1) 
    imshow(img1) 
    
    
    for j=img_begin(i)+1:img_end(i)
        fprintf('\nBegin to process %s 1-%d\n\n', input_path, j);
        
        refstr = num2str(img_begin(i));
        anostr = num2str(j);
        ref_img_file = [input_path 'b' refstr ext];
        ano_img_file = [input_path 'b' anostr ext];
        
        img1 = imread(ano_img_file);
        figure(j) 
        imshow(img1) 
    end    
        

%         ref_region_file = [input_path 'img' refstr '.' detector];
%         ano_region_file = [input_path 'img' anostr '.' detector];
%         
%         homo_file = [input_path 'H' refstr 'to' anostr H_postfix{i}];
%         plot_file = [output_path refstr '-' anostr '.jpg'];
%         plot_title = [sub_dirs{i} ' ' refstr '-' anostr];
%         result_file = [output_path refstr '-' anostr '.txt'];%recall-precision file
%         
%         recall = zeros(2,11);
%         precision = zeros(2,11);
%         match_num = zeros(2,11);
%         correct_num = zeros(2,11);
%         total_corr_num = zeros(2,1);
% 
%         legend_name = cell(1,2);
%         line_spec = cell(1,2);
%         
%         legend_name{1} = 'SIFT';
%         line_spec{1}= '-rx';
%         ref_des_file = [input_path 'img' refstr '.' detector '.sift'];
%         ano_des_file = [input_path 'img' anostr '.' detector '.sift'];
%         [recall(1,:), precision(1,:), match_num(1,:), correct_num(1,:), total_corr_num(1)] = stat_region(ref_des_file,ano_des_file,homo_file,ref_img_file,ano_img_file, MATCH_RN);
%         
%         
%         liop(ano_img_file);
%        
%         ref_des_file = [input_path 'img' refstr '.' detector '.liop'];
%         ano_des_file = [input_path 'img' anostr '.' detector '.liop'];
%         
%         colorId = 1;
%         markerId = 1;
%         idx =  2;
%         
%         colorId = colorId+1;
%         markerId = markerId+1;
%         legend_name{idx} = 'LIOP';
%         line_spec{idx}=['-' color_sty{colorId} marker_sty{markerId}];
%         eval(ref_args_liop);
%         eval(ano_args_liop);
%         [recall(idx,:), precision(idx,:), match_num(idx,:), correct_num(idx,:), total_corr_num(idx)] = stat_region(ref_des_file,ano_des_file,homo_file,ref_img_file,ano_img_file, MATCH_RN);
%         delete(ref_des_file, ano_des_file);
%         
%         %----------------------------plot and save results------------------------
%         save_result(plot_title, plot_file, result_file, precision, recall, match_num, correct_num, total_corr_num, legend_name, line_spec);
%         
%     end
% end
% 
% time = etime(clock, t0);
% hour = int8(time / 3600);
% min = int8(mod(time, 3600)/60);
% sec = mod(mod(time,3600),60);
% fprintf('USING %d hours %d minutes %f seconds\n\n', hour, min, sec);
% 
% % *************************************************************************
% % Stats the recall and precision of the descriptor on affine region
% function [recall, precision, match_num, correct_num, total_corr_num] = stat_region(ref_des_file,ano_des_file,homo_file,ref_img_file,ano_img_file,match_strategy)
% 
% [v_overlap,v_repeatability,v_nb_of_corespondences,matching_score,nb_of_matches,twi] = repeatability(ref_des_file,ano_des_file,homo_file,ref_img_file,ano_img_file,0);
% [correct_match_nn,total_match_nn,correct_match_sim,total_match_sim,correct_match_rn,total_match_rn] = descperf(ref_des_file,ano_des_file,homo_file,ref_img_file,ano_img_file,v_nb_of_corespondences(5),twi);
% 
% 
% switch(match_strategy)
%     case 201
%         total_corr_num = v_nb_of_corespondences(5);
%         match_num = total_match_nn;
%         correct_num = correct_match_nn;
%         recall = correct_match_nn / v_nb_of_corespondences(5);
%         precision = correct_match_nn ./ total_match_nn;
%         
%     case 202
%         total_corr_num = v_nb_of_corespondences(5);
%         match_num = total_match_sim;
%         correct_num = correct_match_sim;
%         recall = correct_match_sim / v_nb_of_corespondences(5);
%         precision = correct_match_sim ./ total_match_sim;
%         
%     case 203
%         total_corr_num = v_nb_of_corespondences(5);
%         match_num = total_match_rn;
%         correct_num = correct_match_rn;
%         recall = correct_match_rn / v_nb_of_corespondences(5);
%         precision = correct_match_rn ./ total_match_rn;
%     otherwise
%         fprintf('Undefined match strategy!\n');


end


% *************************************************************************
% Save the result to figures and text files
function save_result(plot_title, plot_file, result_file, precision, recall, match_num, correct_num, total_corr_num, legend_name, line_spec)

m = size(recall,1);
n = size(recall,2);

%h = figure(1);
h = figure('Position',[1 1280 1280 884], 'PaperPositionMode', 'auto');
set(gcf,'visible','off');

plot(ones(1, n) - precision(1,:), recall(1,:), line_spec{1});
hold on;
for i=2:m
    plot(ones(1, n) - precision(i,:), recall(i,:), line_spec{i});
end
hold off;
xlabel('1-precision');
ylabel('recall');
axis([0 1.0 0 1.0]);
title(plot_title);
legend(legend_name, 'Location','WestOutside');
print(h,'-djpeg','-r0', plot_file);

plot_file_2 = [plot_file(1,1:size(plot_file,2)-4) '.fig'];
set(gcf,'visible','on');
saveas(h, plot_file_2, 'fig');
close;

f = fopen(result_file, 'w');
if f == -1
    error('Could not create file %s', result_file);
end

for i=1:m
    fprintf(f, '%s\ntotal_corr_num = %d\n', legend_name{i}, total_corr_num(i));
    fprintf(f, 'match_num = \n');
    for j=1:n
        fprintf(f,'%d ', match_num(i,j));
    end
    fprintf(f, '\ncorrect_num = \n');
    for j=1:n
        fprintf(f,'%d ', correct_num(i,j));
    end
    fprintf(f, '\nrecall = \n');
    for j=1:n
        fprintf(f,'%f ', recall(i,j));
    end
    fprintf(f, '\nprecision = \n');
    for j=1:n
        fprintf(f,'%f ', precision(i,j));
    end
    fprintf(f, '\n\n');
end

fclose(f);