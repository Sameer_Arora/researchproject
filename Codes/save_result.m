% *************************************************************************
% Save the result to figures and text files

function save_result(plot_title, plot_file, result_file, precision, recall, match_num, correct_num, total_corr_num, legend_name, line_spec)


%h = figure(1);
h = figure('Position',[1 1280 1280 884], 'PaperPositionMode', 'auto');
set(gcf,'visible','off');

hold off;
xlabel('1-precision');
ylabel('recall');
axis([0 1.0 0 1.0]);
title(plot_title);
legend(legend_name, 'Location','WestOutside');
print(h,'-djpeg','-r0', plot_file);

plot_file_2 = [plot_file(1,1:size(plot_file,2)-4) '.fig'];
set(gcf,'visible','on');
close;

f = fopen(result_file, 'w');
if f == -1
    error('Could not create file %s', result_file);
end

for i=1:m
    fprintf(f, '%s\ntotal_corr_num = %d\n', legend_name{i}, total_corr_num(i));
    fprintf(f, 'match_num = \n');
    for j=1:n
        fprintf(f,'%d ', match_num(i,j));
    end
    fprintf(f, '\ncorrect_num = \n');
    for j=1:n
        fprintf(f,'%d ', correct_num(i,j));
    end
    fprintf(f, '\nrecall = \n');
    for j=1:n
        fprintf(f,'%f ', recall(i,j));
    end
    fprintf(f, '\nprecision = \n');
    for j=1:n
        fprintf(f,'%f ', precision(i,j));
    end
    fprintf(f, '\n\n');
end

fclose(f);
end
