/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flood_fill_initialize.c
 *
 * Code generation for function 'flood_fill_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "flood_fill.h"
#include "flood_fill_initialize.h"

/* Function Definitions */
void flood_fill_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (flood_fill_initialize.c) */
