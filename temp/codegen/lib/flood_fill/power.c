/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * power.c
 *
 * Code generation for function 'power'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "flood_fill.h"
#include "power.h"

/* Function Definitions */
void power(const int a[3], int y[3])
{
  int k;
  int b_a;
  int b_y;
  unsigned int bu;
  int exitg1;
  long i2;
  for (k = 0; k < 3; k++) {
    b_a = a[k];
    b_y = 1;
    bu = 2U;
    do {
      exitg1 = 0;
      if ((bu & 1U) != 0U) {
        i2 = (long)b_a * b_y;
        if (i2 > 2147483647L) {
          i2 = 2147483647L;
        } else {
          if (i2 < -2147483648L) {
            i2 = -2147483648L;
          }
        }

        b_y = (int)i2;
      }

      bu >>= 1U;
      if ((int)bu == 0) {
        exitg1 = 1;
      } else {
        i2 = (long)b_a * b_a;
        if (i2 > 2147483647L) {
          i2 = 2147483647L;
        } else {
          if (i2 < -2147483648L) {
            i2 = -2147483648L;
          }
        }

        b_a = (int)i2;
      }
    } while (exitg1 == 0);

    y[k] = b_y;
  }
}

/* End of code generation (power.c) */
