/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * all.h
 *
 * Code generation for function 'all'
 *
 */

#ifndef ALL_H
#define ALL_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "flood_fill_types.h"

/* Function Declarations */
extern boolean_T all(const emxArray_boolean_T *x);

#endif

/* End of code generation (all.h) */
