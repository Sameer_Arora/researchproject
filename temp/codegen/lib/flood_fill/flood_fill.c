/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flood_fill.c
 *
 * Code generation for function 'flood_fill'
 *
 */

/* Include files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "flood_fill.h"
#include "all.h"
#include "flood_fill_emxutil.h"
#include "power.h"

/* Function Declarations */
static double rt_roundd_snf(double u);

/* Function Definitions */
static double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

void flood_fill(const emxArray_uint8_T *I, const double r[2], double tol, const
                double borderColor[3], emxArray_int32_T *ms)
{
  emxArray_uint8_T *R;
  int loop_ub;
  int b_loop_ub;
  int i0;
  emxArray_uint8_T *G;
  int i1;
  emxArray_uint8_T *B;
  int szy;
  int st[20000];
  emxArray_int32_T *ms0;
  double ms0m;
  double stL;
  unsigned int ms0L;
  int R0;
  int G0;
  int B0;
  unsigned int tunableEnvironment_idx_0;
  emxArray_boolean_T *varargout_1;
  int xt;
  int yt;
  boolean_T sku;
  boolean_T skd;
  boolean_T sku1;
  boolean_T skd1;
  int xtt;
  boolean_T exitg1;
  int b_R[3];
  int x[3];
  emxInit_uint8_T(&R, 2);

  /*  flood fill, scan line algoritm for any image not just binary one. */
  /*  ms=flood_fill(I,r,tol) */
  /*   ms - pixels numbers that flooded */
  /*  numbering: number= y+szy*(x-1), x y - pixels coordinaties */
  /*  szx szy - image size */
  /*  I - RGB image */
  /*  r- first point of selection */
  /*  tol - tolerance */
  loop_ub = I->size[0];
  b_loop_ub = I->size[1];
  i0 = R->size[0] * R->size[1];
  R->size[0] = loop_ub;
  R->size[1] = b_loop_ub;
  emxEnsureCapacity_uint8_T(R, i0);
  for (i0 = 0; i0 < b_loop_ub; i0++) {
    for (i1 = 0; i1 < loop_ub; i1++) {
      R->data[i1 + R->size[0] * i0] = I->data[i1 + I->size[0] * i0];
    }
  }

  emxInit_uint8_T(&G, 2);
  loop_ub = I->size[0];
  b_loop_ub = I->size[1];
  i0 = G->size[0] * G->size[1];
  G->size[0] = loop_ub;
  G->size[1] = b_loop_ub;
  emxEnsureCapacity_uint8_T(G, i0);
  for (i0 = 0; i0 < b_loop_ub; i0++) {
    for (i1 = 0; i1 < loop_ub; i1++) {
      G->data[i1 + G->size[0] * i0] = I->data[(i1 + I->size[0] * i0) + I->size[0]
        * I->size[1]];
    }
  }

  emxInit_uint8_T(&B, 2);
  loop_ub = I->size[0];
  b_loop_ub = I->size[1];
  i0 = B->size[0] * B->size[1];
  B->size[0] = loop_ub;
  B->size[1] = b_loop_ub;
  emxEnsureCapacity_uint8_T(B, i0);
  for (i0 = 0; i0 < b_loop_ub; i0++) {
    for (i1 = 0; i1 < loop_ub; i1++) {
      B->data[i1 + B->size[0] * i0] = I->data[(i1 + I->size[0] * i0) + ((I->
        size[0] * I->size[1]) << 1)];
    }
  }

  szy = R->size[0];

  /*  image size */
  /*  stack, where seed will be stored: */
  memset(&st[0], 0, 20000U * sizeof(int));
  emxInit_int32_T(&ms0, 1);
  ms0m = rt_roundd_snf(r[0]);
  if (ms0m < 2.147483648E+9) {
    if (ms0m >= -2.147483648E+9) {
      i0 = (int)ms0m;
    } else {
      i0 = MIN_int32_T;
    }
  } else if (ms0m >= 2.147483648E+9) {
    i0 = MAX_int32_T;
  } else {
    i0 = 0;
  }

  st[0] = i0;
  ms0m = rt_roundd_snf(r[1]);
  if (ms0m < 2.147483648E+9) {
    if (ms0m >= -2.147483648E+9) {
      i0 = (int)ms0m;
    } else {
      i0 = MIN_int32_T;
    }
  } else if (ms0m >= 2.147483648E+9) {
    i0 = MAX_int32_T;
  } else {
    i0 = 0;
  }

  st[10000] = i0;

  /*  r - is start seed */
  stL = 1.0;

  /*  stack length */
  /*  hp=plot(1,1,'.k'); */
  /*   */
  /*  set(hp,'XData',st(1:stL,1),'YData',st(1:stL,2)); */
  /*  drawnow; */
  /*  found pixel store: */
  ms0m = (double)I->size[0] * (double)I->size[1];

  /*  margin */
  i0 = ms0->size[0];
  ms0->size[0] = (int)ms0m;
  emxEnsureCapacity_int32_T(ms0, i0);
  loop_ub = (int)ms0m;
  for (i0 = 0; i0 < loop_ub; i0++) {
    ms0->data[i0] = 0;
  }

  /*  predifined array to increase speed */
  ms0L = 0U;

  /*  0 points initially */
  /*  initial point color: */
  ms0m = rt_roundd_snf(borderColor[0]);
  if (ms0m < 2.147483648E+9) {
    if (ms0m >= -2.147483648E+9) {
      R0 = (int)ms0m;
    } else {
      R0 = MIN_int32_T;
    }
  } else if (ms0m >= 2.147483648E+9) {
    R0 = MAX_int32_T;
  } else {
    R0 = 0;
  }

  ms0m = rt_roundd_snf(borderColor[1]);
  if (ms0m < 2.147483648E+9) {
    if (ms0m >= -2.147483648E+9) {
      G0 = (int)ms0m;
    } else {
      G0 = MIN_int32_T;
    }
  } else if (ms0m >= 2.147483648E+9) {
    G0 = MAX_int32_T;
  } else {
    G0 = 0;
  }

  ms0m = rt_roundd_snf(borderColor[2]);
  if (ms0m < 2.147483648E+9) {
    if (ms0m >= -2.147483648E+9) {
      B0 = (int)ms0m;
    } else {
      B0 = MIN_int32_T;
    }
  } else if (ms0m >= 2.147483648E+9) {
    B0 = MAX_int32_T;
  } else {
    B0 = 0;
  }

  /*  function to convet input to pixel number */
  tunableEnvironment_idx_0 = (unsigned int)R->size[0];
  emxInit_boolean_T(&varargout_1, 1);
  do {
    /*  get seed from stack: */
    xt = st[(int)stL - 1];
    yt = st[(int)stL + 9999];
    stL--;

    /*  line: */
    /*     %% To right */
    sku = false;

    /*  seed key, true if seed added, up */
    skd = false;

    /*  same for down */
    sku1 = false;

    /*  this key is need to prewnt extra seed when move to left, up */
    skd1 = false;

    /*  same for left */
    i0 = R->size[1];
    xtt = xt;
    exitg1 = false;
    while ((!exitg1) && (xtt <= i0)) {
      b_loop_ub = R->data[(yt + R->size[0] * (xtt - 1)) - 1];
      if (R0 < b_loop_ub - MAX_int32_T) {
        b_loop_ub = MAX_int32_T;
      } else {
        b_loop_ub -= R0;
      }

      b_R[0] = b_loop_ub;
      b_loop_ub = G->data[(yt + G->size[0] * (xtt - 1)) - 1];
      if (G0 < b_loop_ub - MAX_int32_T) {
        b_loop_ub = MAX_int32_T;
      } else {
        b_loop_ub -= G0;
      }

      b_R[1] = b_loop_ub;
      b_loop_ub = B->data[(yt + B->size[0] * (xtt - 1)) - 1];
      if (B0 < b_loop_ub - MAX_int32_T) {
        b_loop_ub = MAX_int32_T;
      } else {
        b_loop_ub -= B0;
      }

      b_R[2] = b_loop_ub;
      power(b_R, x);
      ms0m = x[0];
      for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
        ms0m += (double)x[b_loop_ub + 1];
      }

      if (ms0m >= tol) {
        /*  add pixel */
        ms0L++;
        ms0m = (double)tunableEnvironment_idx_0 * ((double)xtt - 1.0);
        if (ms0m < 2.147483648E+9) {
          if (ms0m >= -2.147483648E+9) {
            loop_ub = (int)ms0m;
          } else {
            loop_ub = MIN_int32_T;
          }
        } else {
          loop_ub = MAX_int32_T;
        }

        if ((yt < 0) && (loop_ub < MIN_int32_T - yt)) {
          b_loop_ub = MIN_int32_T;
        } else if ((yt > 0) && (loop_ub > MAX_int32_T - yt)) {
          b_loop_ub = MAX_int32_T;
        } else {
          b_loop_ub = yt + loop_ub;
        }

        ms0->data[(int)ms0L - 1] = b_loop_ub;

        /*  try to add seed up: */
        if (yt != szy) {
          if (yt > 2147483646) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub = yt + 1;
          }

          b_loop_ub = R->data[(b_loop_ub + R->size[0] * (xtt - 1)) - 1];
          if (R0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= R0;
          }

          b_R[0] = b_loop_ub;
          if (yt > 2147483646) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub = yt + 1;
          }

          b_loop_ub = G->data[(b_loop_ub + G->size[0] * (xtt - 1)) - 1];
          if (G0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= G0;
          }

          b_R[1] = b_loop_ub;
          if (yt > 2147483646) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub = yt + 1;
          }

          b_loop_ub = B->data[(b_loop_ub + B->size[0] * (xtt - 1)) - 1];
          if (B0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= B0;
          }

          b_R[2] = b_loop_ub;
          power(b_R, x);
          ms0m = x[0];
          for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
            ms0m += (double)x[b_loop_ub + 1];
          }

          if (ms0m >= tol) {
            if (!sku) {
              /*                      tn(xtt,yt+1) */
              /*                      ms0(1:ms0L) */
              /*                      tn(xtt,yt+1)~= ms0(1:ms0L) */
              if (yt > 2147483646) {
                b_loop_ub = MAX_int32_T;
              } else {
                b_loop_ub = yt + 1;
              }

              ms0m = (double)tunableEnvironment_idx_0 * ((double)xtt - 1.0);
              if (ms0m < 2.147483648E+9) {
                if (ms0m >= -2.147483648E+9) {
                  loop_ub = (int)ms0m;
                } else {
                  loop_ub = MIN_int32_T;
                }
              } else {
                loop_ub = MAX_int32_T;
              }

              if ((b_loop_ub < 0) && (loop_ub < MIN_int32_T - b_loop_ub)) {
                b_loop_ub = MIN_int32_T;
              } else if ((b_loop_ub > 0) && (loop_ub > MAX_int32_T - b_loop_ub))
              {
                b_loop_ub = MAX_int32_T;
              } else {
                b_loop_ub += loop_ub;
              }

              i1 = varargout_1->size[0];
              varargout_1->size[0] = (int)ms0L;
              emxEnsureCapacity_boolean_T(varargout_1, i1);
              loop_ub = (int)ms0L;
              for (i1 = 0; i1 < loop_ub; i1++) {
                varargout_1->data[i1] = (b_loop_ub != ms0->data[i1]);
              }

              if (all(varargout_1)) {
                /*  if free space */
                /*  add to stack */
                stL++;
                st[(int)stL - 1] = xtt;
                if (yt > 2147483646) {
                  b_loop_ub = MAX_int32_T;
                } else {
                  b_loop_ub = yt + 1;
                }

                st[(int)stL + 9999] = b_loop_ub;
                sku = true;
              }
            }
          } else {
            sku = false;
          }

          if (xtt == xt) {
            sku1 = sku;

            /*  memorize, will be used when to left */
          }
        }

        /*  try to add down: */
        if (yt != 1) {
          b_loop_ub = R->data[(yt + R->size[0] * (xtt - 1)) - 2];
          if (R0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= R0;
          }

          b_R[0] = b_loop_ub;
          b_loop_ub = G->data[(yt + G->size[0] * (xtt - 1)) - 2];
          if (G0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= G0;
          }

          b_R[1] = b_loop_ub;
          b_loop_ub = B->data[(yt + B->size[0] * (xtt - 1)) - 2];
          if (B0 < b_loop_ub - MAX_int32_T) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub -= B0;
          }

          b_R[2] = b_loop_ub;
          power(b_R, x);
          ms0m = x[0];
          for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
            ms0m += (double)x[b_loop_ub + 1];
          }

          if (ms0m >= tol) {
            if (!skd) {
              b_loop_ub = yt - 1;
              ms0m = (double)tunableEnvironment_idx_0 * ((double)xtt - 1.0);
              if (ms0m < 2.147483648E+9) {
                if (ms0m >= -2.147483648E+9) {
                  loop_ub = (int)ms0m;
                } else {
                  loop_ub = MIN_int32_T;
                }
              } else {
                loop_ub = MAX_int32_T;
              }

              if ((b_loop_ub < 0) && (loop_ub < MIN_int32_T - b_loop_ub)) {
                b_loop_ub = MIN_int32_T;
              } else if ((b_loop_ub > 0) && (loop_ub > MAX_int32_T - b_loop_ub))
              {
                b_loop_ub = MAX_int32_T;
              } else {
                b_loop_ub += loop_ub;
              }

              i1 = varargout_1->size[0];
              varargout_1->size[0] = (int)ms0L;
              emxEnsureCapacity_boolean_T(varargout_1, i1);
              loop_ub = (int)ms0L;
              for (i1 = 0; i1 < loop_ub; i1++) {
                varargout_1->data[i1] = (b_loop_ub != ms0->data[i1]);
              }

              if (all(varargout_1)) {
                /*  if free space */
                /*  add to stack */
                stL++;
                st[(int)stL - 1] = xtt;
                st[(int)stL + 9999] = yt - 1;
                skd = true;
              }
            }
          } else {
            skd = false;
          }

          if (xtt == xt) {
            skd1 = skd;

            /*  memorize, will be used when to left */
          }
        }

        xtt++;
      } else {
        exitg1 = true;
      }
    }

    /*     %% to left */
    /* sku=false; % seed key, true if seed added */
    /* skd=false; */
    sku = sku1;
    skd = skd1;
    if (xt != 1) {
      if (xt < -2147483647) {
        b_loop_ub = MIN_int32_T;
      } else {
        b_loop_ub = xt - 1;
      }

      xtt = b_loop_ub - 1;
      exitg1 = false;
      while ((!exitg1) && (xtt + 1 > 0)) {
        b_loop_ub = R->data[(yt + R->size[0] * xtt) - 1];
        if (R0 < b_loop_ub - MAX_int32_T) {
          b_loop_ub = MAX_int32_T;
        } else {
          b_loop_ub -= R0;
        }

        b_R[0] = b_loop_ub;
        b_loop_ub = G->data[(yt + G->size[0] * xtt) - 1];
        if (G0 < b_loop_ub - MAX_int32_T) {
          b_loop_ub = MAX_int32_T;
        } else {
          b_loop_ub -= G0;
        }

        b_R[1] = b_loop_ub;
        b_loop_ub = B->data[(yt + B->size[0] * xtt) - 1];
        if (B0 < b_loop_ub - MAX_int32_T) {
          b_loop_ub = MAX_int32_T;
        } else {
          b_loop_ub -= B0;
        }

        b_R[2] = b_loop_ub;
        power(b_R, x);
        ms0m = x[0];
        for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
          ms0m += (double)x[b_loop_ub + 1];
        }

        if (ms0m >= tol) {
          /*  add pixel */
          ms0L++;
          ms0m = (double)tunableEnvironment_idx_0 * ((double)(xtt + 1) - 1.0);
          if (ms0m < 2.147483648E+9) {
            if (ms0m >= -2.147483648E+9) {
              loop_ub = (int)ms0m;
            } else {
              loop_ub = MIN_int32_T;
            }
          } else {
            loop_ub = MAX_int32_T;
          }

          if ((yt < 0) && (loop_ub < MIN_int32_T - yt)) {
            b_loop_ub = MIN_int32_T;
          } else if ((yt > 0) && (loop_ub > MAX_int32_T - yt)) {
            b_loop_ub = MAX_int32_T;
          } else {
            b_loop_ub = yt + loop_ub;
          }

          ms0->data[(int)ms0L - 1] = b_loop_ub;

          /*  try to add seed up: */
          if (yt != szy) {
            if (yt > 2147483646) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub = yt + 1;
            }

            b_loop_ub = R->data[(b_loop_ub + R->size[0] * xtt) - 1];
            if (R0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= R0;
            }

            b_R[0] = b_loop_ub;
            if (yt > 2147483646) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub = yt + 1;
            }

            b_loop_ub = G->data[(b_loop_ub + G->size[0] * xtt) - 1];
            if (G0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= G0;
            }

            b_R[1] = b_loop_ub;
            if (yt > 2147483646) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub = yt + 1;
            }

            b_loop_ub = B->data[(b_loop_ub + B->size[0] * xtt) - 1];
            if (B0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= B0;
            }

            b_R[2] = b_loop_ub;
            power(b_R, x);
            ms0m = x[0];
            for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
              ms0m += (double)x[b_loop_ub + 1];
            }

            if (ms0m >= tol) {
              if (!sku) {
                if (yt > 2147483646) {
                  b_loop_ub = MAX_int32_T;
                } else {
                  b_loop_ub = yt + 1;
                }

                ms0m = (double)tunableEnvironment_idx_0 * ((double)(xtt + 1) -
                  1.0);
                if (ms0m < 2.147483648E+9) {
                  if (ms0m >= -2.147483648E+9) {
                    loop_ub = (int)ms0m;
                  } else {
                    loop_ub = MIN_int32_T;
                  }
                } else {
                  loop_ub = MAX_int32_T;
                }

                if ((b_loop_ub < 0) && (loop_ub < MIN_int32_T - b_loop_ub)) {
                  b_loop_ub = MIN_int32_T;
                } else if ((b_loop_ub > 0) && (loop_ub > MAX_int32_T - b_loop_ub))
                {
                  b_loop_ub = MAX_int32_T;
                } else {
                  b_loop_ub += loop_ub;
                }

                i0 = varargout_1->size[0];
                varargout_1->size[0] = (int)ms0L;
                emxEnsureCapacity_boolean_T(varargout_1, i0);
                loop_ub = (int)ms0L;
                for (i0 = 0; i0 < loop_ub; i0++) {
                  varargout_1->data[i0] = (b_loop_ub != ms0->data[i0]);
                }

                if (all(varargout_1)) {
                  /*  if free space */
                  /*  add to stack */
                  stL++;
                  st[(int)stL - 1] = xtt + 1;
                  if (yt > 2147483646) {
                    b_loop_ub = MAX_int32_T;
                  } else {
                    b_loop_ub = yt + 1;
                  }

                  st[(int)stL + 9999] = b_loop_ub;
                  sku = true;
                }
              }
            } else {
              sku = false;
            }
          }

          /*  try to add down */
          if (yt != 1) {
            b_loop_ub = R->data[(yt + R->size[0] * xtt) - 2];
            if (R0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= R0;
            }

            b_R[0] = b_loop_ub;
            b_loop_ub = G->data[(yt + G->size[0] * xtt) - 2];
            if (G0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= G0;
            }

            b_R[1] = b_loop_ub;
            b_loop_ub = B->data[(yt + B->size[0] * xtt) - 2];
            if (B0 < b_loop_ub - MAX_int32_T) {
              b_loop_ub = MAX_int32_T;
            } else {
              b_loop_ub -= B0;
            }

            b_R[2] = b_loop_ub;
            power(b_R, x);
            ms0m = x[0];
            for (b_loop_ub = 0; b_loop_ub < 2; b_loop_ub++) {
              ms0m += (double)x[b_loop_ub + 1];
            }

            if (ms0m >= tol) {
              if (!skd) {
                b_loop_ub = yt - 1;
                ms0m = (double)tunableEnvironment_idx_0 * ((double)(xtt + 1) -
                  1.0);
                if (ms0m < 2.147483648E+9) {
                  if (ms0m >= -2.147483648E+9) {
                    loop_ub = (int)ms0m;
                  } else {
                    loop_ub = MIN_int32_T;
                  }
                } else {
                  loop_ub = MAX_int32_T;
                }

                if ((b_loop_ub < 0) && (loop_ub < MIN_int32_T - b_loop_ub)) {
                  b_loop_ub = MIN_int32_T;
                } else if ((b_loop_ub > 0) && (loop_ub > MAX_int32_T - b_loop_ub))
                {
                  b_loop_ub = MAX_int32_T;
                } else {
                  b_loop_ub += loop_ub;
                }

                i0 = varargout_1->size[0];
                varargout_1->size[0] = (int)ms0L;
                emxEnsureCapacity_boolean_T(varargout_1, i0);
                loop_ub = (int)ms0L;
                for (i0 = 0; i0 < loop_ub; i0++) {
                  varargout_1->data[i0] = (b_loop_ub != ms0->data[i0]);
                }

                if (all(varargout_1)) {
                  /*  if free space */
                  /*  add to stack */
                  stL++;
                  st[(int)stL - 1] = xtt + 1;
                  st[(int)stL + 9999] = yt - 1;
                  skd = true;
                }
              }
            } else {
              skd = false;
            }
          }

          xtt--;
        } else {
          exitg1 = true;
        }
      }
    }

    /*    %% Completion  */
    /*      set(hp,'XData',st(1:stL,1),'YData',st(1:stL,2)); */
    /*      drawnow; */
    /*      pause(1); */
    /*      noadded=ms0L */
    /*  code to chechk the results . */
    /*      cl=[0; 255; 255];% color to fill */
    /*      img=I; */
    /*      subplot(1,2,1); */
    /*      imagesc(I); */
    /*      impixelinfo; */
    /*      title('Original'); */
    /*       */
    /*      R1=img(:,:,1); */
    /*      G1=img(:,:,2); */
    /*      B1=img(:,:,3); */
    /*     ms=ms0(1:ms0L); */
    /*      R1(ms)=cl(1); */
    /*      G1(ms)=cl(2); */
    /*      B1(ms)=cl(3); */
    /*   */
    /*      img(:,:,1)=R1; */
    /*      img(:,:,2)=G1; */
    /*      img(:,:,3)=B1; */
    /*      subplot(1,2,2); */
    /*      imagesc(img); */
    /*      impixelinfo; */
    /*      title('flood fill'); */
  } while (!(stL == 0.0));

  emxFree_boolean_T(&varargout_1);
  emxFree_uint8_T(&B);
  emxFree_uint8_T(&G);
  emxFree_uint8_T(&R);

  /*  no more seed */
  if (1 > (int)ms0L) {
    loop_ub = 0;
  } else {
    loop_ub = (int)ms0L;
  }

  i0 = ms->size[0];
  ms->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(ms, i0);
  for (i0 = 0; i0 < loop_ub; i0++) {
    ms->data[i0] = ms0->data[i0];
  }

  emxFree_int32_T(&ms0);
}

/* End of code generation (flood_fill.c) */
