/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * main.c
 *
 * Code generation for function 'main'
 *
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include files */
#include "rt_nonfinite.h"
#include "flood_fill.h"
#include "main.h"
#include "flood_fill_terminate.h"
#include "flood_fill_emxAPI.h"
#include "flood_fill_initialize.h"

/* Function Declarations */
static void argInit_1x1x3_real_T(double result[3]);
static void argInit_1x2_real_T(double result[2]);
static double argInit_real_T(void);
static unsigned char argInit_uint8_T(void);
static emxArray_uint8_T *c_argInit_UnboundedxUnboundedx3(void);
static void main_flood_fill(void);

/* Function Definitions */
static void argInit_1x1x3_real_T(double result[3])
{
  int idx2;

  /* Loop over the array to initialize each element. */
  for (idx2 = 0; idx2 < 3; idx2++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx2] = argInit_real_T();
  }
}

static void argInit_1x2_real_T(double result[2])
{
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 2; idx1++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx1] = argInit_real_T();
  }
}

static double argInit_real_T(void)
{
  return 0.0;
}

static unsigned char argInit_uint8_T(void)
{
  return 0U;
}

static emxArray_uint8_T *c_argInit_UnboundedxUnboundedx3(void)
{
  emxArray_uint8_T *result;
  static int iv0[3] = { 2, 2, 3 };

  int idx0;
  int idx1;
  int idx2;

  /* Set the size of the array.
     Change this size to the value that the application requires. */
  result = emxCreateND_uint8_T(3, iv0);

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < result->size[0U]; idx0++) {
    for (idx1 = 0; idx1 < result->size[1U]; idx1++) {
      for (idx2 = 0; idx2 < 3; idx2++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result->data[(idx0 + result->size[0] * idx1) + result->size[0] *
          result->size[1] * idx2] = argInit_uint8_T();
      }
    }
  }

  return result;
}

static void main_flood_fill(void)
{
  emxArray_int32_T *ms;
  emxArray_uint8_T *I;
  double dv0[2];
  double dv1[3];
  emxInitArray_int32_T(&ms, 1);

  /* Initialize function 'flood_fill' input arguments. */
  /* Initialize function input argument 'I'. */
  I = c_argInit_UnboundedxUnboundedx3();

  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'borderColor'. */
  /* Call the entry-point 'flood_fill'. */
  argInit_1x2_real_T(dv0);
  argInit_1x1x3_real_T(dv1);
  flood_fill(I, dv0, argInit_real_T(), dv1, ms);
  emxDestroyArray_int32_T(ms);
  emxDestroyArray_uint8_T(I);
}

int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* Initialize the application.
     You do not need to do this more than one time. */
  flood_fill_initialize();

  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_flood_fill();

  /* Terminate the application.
     You do not need to do this more than one time. */
  flood_fill_terminate();
  return 0;
}

/* End of code generation (main.c) */
