/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_flood_fill_api.h
 *
 * Code generation for function '_coder_flood_fill_api'
 *
 */

#ifndef _CODER_FLOOD_FILL_API_H
#define _CODER_FLOOD_FILL_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_flood_fill_api.h"

/* Type Definitions */
#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T

typedef struct emxArray_int32_T emxArray_int32_T;

#endif                                 /*typedef_emxArray_int32_T*/

#ifndef struct_emxArray_uint8_T
#define struct_emxArray_uint8_T

struct emxArray_uint8_T
{
  uint8_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_uint8_T*/

#ifndef typedef_emxArray_uint8_T
#define typedef_emxArray_uint8_T

typedef struct emxArray_uint8_T emxArray_uint8_T;

#endif                                 /*typedef_emxArray_uint8_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void flood_fill(emxArray_uint8_T *I, real_T r[2], real_T tol, real_T
  borderColor[3], emxArray_int32_T *ms);
extern void flood_fill_api(const mxArray * const prhs[4], int32_T nlhs, const
  mxArray *plhs[1]);
extern void flood_fill_atexit(void);
extern void flood_fill_initialize(void);
extern void flood_fill_terminate(void);
extern void flood_fill_xil_terminate(void);

#endif

/* End of code generation (_coder_flood_fill_api.h) */
