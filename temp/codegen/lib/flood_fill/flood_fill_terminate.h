/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flood_fill_terminate.h
 *
 * Code generation for function 'flood_fill_terminate'
 *
 */

#ifndef FLOOD_FILL_TERMINATE_H
#define FLOOD_FILL_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "flood_fill_types.h"

/* Function Declarations */
extern void flood_fill_terminate(void);

#endif

/* End of code generation (flood_fill_terminate.h) */
