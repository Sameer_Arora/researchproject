/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * flood_fill.h
 *
 * Code generation for function 'flood_fill'
 *
 */

#ifndef FLOOD_FILL_H
#define FLOOD_FILL_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "flood_fill_types.h"

/* Function Declarations */
extern void flood_fill(const emxArray_uint8_T *I, const double r[2], double tol,
  const double borderColor[3], emxArray_int32_T *ms);

#endif

/* End of code generation (flood_fill.h) */
